#!/usr/bin/env bash

rm -rf ./src-php
rm -rf ./src/main/*

thrift -r --gen php:server yumall.thrift.model.thrift && \
thrift -r --gen php:server yumall.thrift.address.thrift && \
thrift -r --gen php:server yumall.thrift.favorites.thrift && \
thrift -r --gen php:server yumall.thrift.products.thrift && \
thrift -r --gen php:server yumall.thrift.order.thrift && \
mv gen-php src-php

thrift -r --gen java -o src/main yumall.thrift.address.thrift && \
thrift -r --gen java -o src/main yumall.thrift.products.thrift && \
thrift -r --gen java -o src/main yumall.thrift.order.thrift && \
mv src/main/gen-java src/main/java


