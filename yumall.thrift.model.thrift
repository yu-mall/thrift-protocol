namespace java shop.yumall.thrift.model
namespace py yumall.thrift.model
namespace php Yumall.Thrift.Model

struct User {
    1: required string id;
    3: optional string type;
}

struct Page {
    1: optional i32 total; // 总数量
    2: optional i32 size; // 一页大小
    3: optional i32 current; // 当前叶，从1开始
    4: optional map<string,string> query; 
}