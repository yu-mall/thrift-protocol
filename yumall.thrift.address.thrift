namespace java shop.yumall.thrift.address
namespace py yumall.thrift.address
namespace php Yumall.Thrift.Address

struct Address {
    1: required i64 id;
    2: required i64 userId;
    3: required string name;
    4: required string telephone;
    5: required string province; // 省
    6: required string city; // 市
    7: required string area; // 区
    8: required string detailed;
    9: required bool isDefault;
}

service AddressService {
    list<Address> get(1: i64 addressId);
    list<Address> getList(1: i64 userId);
    bool add(1: Address address);
    bool remove(1: Address address);
    bool setDefault(1: i64 addressId)
}

