namespace java shop.yumall.thrift.favorite
namespace php Yumall.Thrift.Favorite

struct Favorite {
    1: required string sku;
    2: required string userId;
}

service FavoriteService {
    list<string> get(1: string userId);
    bool add(1: Favorite request);
    bool remove(1: Favorite request);
}