include "yumall.thrift.model.thrift"

namespace java shop.yumall.thrift.order
namespace php Yumall.Thrift.Order

enum Status{
    /** @var int 待付款 */
    PENDING_PAYMENT = 100;
    /** @var int 待发货 */
    PENDING_DELIVERED = 101;
    /** @var int 待收货 */
    PENDING_RECEIPT = 300;
    /** @var int 已完成 */
    COMPLETED = 200;
    /** @var int 已取消 */
    CANCEL = 400;
    /** @var int 支付超时 */
    TIME_OUT = 401;
}

struct Order {
    1: required string orderNo; // 订单号
    2: required string userId; // 用户ID
    3: required string createAt; // 订单创建时间
    4: required double amountTotal; // 订单总金额
    5: required string name; // 收货人
    6: required string address; // 收货地址
    7: required string phone; // 联系方式
    8: required Status status;
    9: required i16 count;
    10: optional string remarks;
    11: optional set<OrderItem> items;
}

struct OrderItem{
    1: required string orderNo;
    2: required string orderItemNo;
    3: required string sku;
    4: required double amount;
}

struct Inquire{
    1: optional Status status;
    2: required i32 count;
    3: required i32 page;
}

exception Exception{
    1: string errMessage;
}

service OrderService {
    list<Order> getOrderListByUser(1: string userId, 2: Inquire inquire);
    list<Order> getOrderList(1: Inquire inquire);
    list<Order> getDetail(1: string orderNo, 2: Status status);
    bool create(1: Order order) throws (1:Exception ex);
    bool update(1: Order order) throws (1:Exception ex);
}