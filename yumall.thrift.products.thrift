include "yumall.thrift.model.thrift"

namespace java shop.yumall.thrift.products
namespace py yumall.thrift.products
namespace php Yumall.Thrift.Products

struct Category {
    1: optional Category upper; // 上级分类
    2: required string code; // 分类唯一编码
    3: required string name; // 分类名称
    4: required string type; // 类型 1: 节点分类 2： 叶子分类
    5: optional list<Category> child; // 子分类
}

enum AttributeType {
    FEATURE = 100;
    SPU_ATTRIBUTE = 200;
    SKU_ATTRIBUTE = 201;
}

struct Attribute {
    1: required i64 code;
    2: required string name;
    3: required AttributeType type;
}

struct AttributeSet {
    1: required i64 code;
    2: required string value;
}

struct Spu {
    1: required string spu;
    2: required string name;
    3: required Category category;
    4: optional set<Sku> skus;
    5: optional set<AttributeSet> setList;
}

struct Sku {
    1: required string sku;
    2: required Spu spu;
    3: required set<AttributeSet> setList;
    4: optional list<string> imgList;
}

struct Image {
    1: required string url;
    2: required string type;
}

service ProductsService {
    Category getCategory(1: string code);
    bool addCategory(1: Category category);
    bool delCategory(1: Category category);
    Category updateCategory(1: Category category);

    bool addSpu(1: Spu spu);
    bool delSpu(1: string spuCode)
    Spu getSpu(1: string spuCode);
    list<Spu> getSpuList(1: yumall.thrift.model.Page page);
    Spu updateSpu(1: Spu spu);
    
    bool addSku(1: Spu sku);
    bool delSku(1: string skuCode)
    Spu getSku(1: string skuCode);
    list<Spu> getSkuList(1: yumall.thrift.model.Page page);
    Spu updateSku(1: Sku sku);
}

